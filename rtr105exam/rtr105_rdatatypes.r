# This r script contains all the written assessments of the section 1.3
library("dslabs")

# Use the function names to extract the variable names
names(murders)

# Use the accessor to extrract state abreviations and assigne it to a
a <- murders$abb
# Determine the class of a
class(a)

# Use square brackets to extract `abb` from `murders` and assign it to b
b <- murders[["abb"]]
# Check if `a` and `b` are identical
identical(a,b)

# Determine the number of regions included in the variable
lenght(levels(murders$region))

# Write one line of code to show the number of states per region
table(murders$region)

