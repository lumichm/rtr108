# This is the assessments script that contains all the assessments of Section 1.2

# Below, write code to calculate the sum of the first 100 integers
n <- 100
n*(n+1)/2

# Below, write code to calculate the sum of the first 1000 integers
n <- 1000
n*(n+1)/2
